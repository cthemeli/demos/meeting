package gr.demokritos.meeting;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class DataCollector {
    public static void main(String[] args) {
        DataCollector dataCollector = new DataCollector();
        Scanner scanner = new Scanner(System.in);
        System.out.println("If you want to add data press 1");
        System.out.println("If you want to delete the content of the file press 2");
        System.out.println("If you want to create a new meeting press 3");
        System.out.println("If you want to delete a file press 4");
        System.out.println("If you want to exit press q");
        String input = scanner.nextLine();
        while (!input.equalsIgnoreCase("q")) {
            if (input.equals("1")) {
                dataCollector.appendFile();
            } else if (input.equals("2")) {
                try {
                    dataCollector.deleteFileContent();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (input.equals("3")) {
                dataCollector.createNewFile();
            } else if (input.equals("4")) {
                dataCollector.deleteFile();
            } else {
                System.out.println("Wrong input!!");
            }

            scanner = new Scanner(System.in);
            System.out.println("If you want to add data press 1");
            System.out.println("If you want to delete the content of the file press 2");
            System.out.println("If you want to create a new meeting press 3");
            System.out.println("If you want to delete a file press 4");
            System.out.println("If you want to exit press q");
            input = scanner.nextLine();
        }
    }

    public void createNewFile() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give the name of the meeting");
        String input = scanner.nextLine();
        Path newFilePath = Paths.get("src/main/resources/" + input + ".txt");
        try {
            Files.createFile(newFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteFile() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give the name of the meeting");
        String input = scanner.nextLine();
        Path fileToDeletePath = Paths.get("src/main/resources/" + input + ".txt");
        try {
            Files.delete(fileToDeletePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteFileContent() throws NullPointerException, FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give the name of the meeting");
        String input = scanner.nextLine();
        File file = new File("src/main/resources" + input + ".txt");
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    private void appendFile() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give the name of the meeting for which you are interested");
        String input = scanner.nextLine();
        String fileName = input + ".txt";
        scanner = new Scanner(System.in);
        System.out.println("Give the meeting's duration");
        input = scanner.nextLine();
        String duration = input;
        scanner = new Scanner(System.in);
        System.out.println("Give new data about the members to attend this week's meeting. Press q to exit");
        input = scanner.nextLine();
        while (!input.equalsIgnoreCase("q")) {
            List<DocumentLine> lines = processInput(input, duration);
            writeToFile(fileName, lines);
            scanner = new Scanner(System.in);
            System.out.println("Give new data about the members to attend this week's meeting or press q to exit");
            input = scanner.nextLine();
        }
    }

    private List<DocumentLine> processInput(String input, String duration) {
        List<String> inputParts = Arrays.asList(input.split(" "));
        List<DocumentLine> lines = new ArrayList<>();
        String name = inputParts.get(0);
        String date = "";
        String timezone;
        String availability = "";
        int i = 1;
        while (i < inputParts.size()) {
            if (isDay(inputParts.get(i))) {
                date = findDayOfThisWeek(normalizeDay(inputParts.get(i)));
                i++;
            } else if (inputParts.get(i).equalsIgnoreCase("this")) {
                i++;
                date = findDayOfThisWeek(normalizeDay(inputParts.get(i)));
            } else if (inputParts.get(i).equalsIgnoreCase("next")) {
                i++;
                date = findDayOfNextWeek(inputParts, i);
            } else if (inputParts.get(i).equalsIgnoreCase("can")) {
                availability = "true";
                i++;
            } else if (inputParts.get(i).equalsIgnoreCase("cannot") ||
                    inputParts.get(i).equalsIgnoreCase("can't") ||
                    input.equalsIgnoreCase("cant")) {
                availability = "false";
                i++;
            } else if (inputParts.get(i).equalsIgnoreCase("before")) {
                i++;
                LocalTime endTime = LocalTime.parse(inputParts.get(i));
                LocalTime startTime = endTime.minus(Integer.parseInt(duration), ChronoUnit.HOURS);
                LocalTime minorTime = LocalTime.parse("09:00");
                while (!startTime.isBefore(minorTime)) {
                    timezone = startTime.toString() + "-" + endTime.toString();
                    DocumentLine documentLine = new DocumentLine(name, date, timezone, availability);
                    lines.add(documentLine);
                    endTime = endTime.minus(1, ChronoUnit.HOURS);
                    startTime = endTime.minus(Integer.parseInt(duration), ChronoUnit.HOURS);
                }
                i++;
            } else if (inputParts.get(i).equalsIgnoreCase("after")) {
                i++;
                LocalTime startTime = LocalTime.parse(inputParts.get(i));
                LocalTime endTime = startTime.plus(Integer.parseInt(duration), ChronoUnit.HOURS);
                LocalTime maxTime = LocalTime.parse("21:00");
                while (!endTime.isAfter(maxTime)) {
                    timezone = startTime.toString() + "-" + endTime.toString();
                    DocumentLine documentLine = new DocumentLine(name, date, timezone, availability);
                    lines.add(documentLine);
                    startTime = startTime.plus(1, ChronoUnit.HOURS);
                    endTime = startTime.plus(Integer.parseInt(duration), ChronoUnit.HOURS);
                }
                i++;
            } else {
                List<String> timezoneParts = Arrays.asList(inputParts.get(i).split("-"));
                LocalTime startTime = LocalTime.parse(timezoneParts.get(0));
                LocalTime endTime = LocalTime.parse(timezoneParts.get(1));
                LocalTime testTime = startTime.plus(Integer.parseInt(duration), ChronoUnit.HOURS);
                while (!testTime.isAfter(endTime)) {
                    timezone = startTime.toString() + "-" + testTime.toString();
                    DocumentLine documentLine = new DocumentLine(name, date, timezone, availability);
                    lines.add(documentLine);
                    startTime = startTime.plus(1, ChronoUnit.HOURS);
                    testTime = startTime.plus(Integer.parseInt(duration), ChronoUnit.HOURS);
                }
                i++;
            }
        }
        return lines;
    }

    private boolean isDay(String input) {
        return input.equalsIgnoreCase("monday") || input.equalsIgnoreCase("mon") || input.equalsIgnoreCase("tuesday") ||
                input.equalsIgnoreCase("tue") || input.equalsIgnoreCase("wed") || input.equalsIgnoreCase("wednesday") ||
                input.equalsIgnoreCase("thu") || input.equalsIgnoreCase("thursday") || input.equalsIgnoreCase("fri") ||
                input.equalsIgnoreCase("friday");
    }

    private String normalizeDay(String input) {
        if (input.equalsIgnoreCase("mon")) {
            return "monday";
        } else if (input.equalsIgnoreCase("tue")) {
            return "tuesday";
        } else if (input.equalsIgnoreCase("wed")) {
            return "wednesday";
        } else if (input.equalsIgnoreCase("thu")) {
            return "thursday";
        } else if (input.equalsIgnoreCase("fri")) {
            return "friday";
        }
        return input;
    }

    private String findDayOfNextWeek(List<String> inputPars, int i) {
        String date = "";
        int count = 0;
        String day = inputPars.get(i);
        day = normalizeDay(day);
        String currentDay = "";
        LocalDate today = LocalDate.now();
        while (!currentDay.equalsIgnoreCase(day)) {
            currentDay = today.getDayOfWeek().toString();
            if (currentDay.equalsIgnoreCase(day)) {
                if (count == 0) {
                    count++;
                    today = today.plus(1, ChronoUnit.DAYS);
                    currentDay = today.getDayOfWeek().toString();
                } else {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                    date = today.format(formatter);
                }
            } else {
                today = today.plus(1, ChronoUnit.DAYS);
            }
        }
        return date;
    }

    private String findDayOfThisWeek(String day) {
        String date = "";
        String currentDay = "";
        LocalDate today = LocalDate.now();
        while (!currentDay.equalsIgnoreCase(day)) {
            currentDay = today.getDayOfWeek().toString();
            if (currentDay.equalsIgnoreCase(day)) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                date = today.format(formatter);
            } else {
                today = today.plus(1, ChronoUnit.DAYS);
            }
        }
        return date;
    }

    private void writeToFile(String fileName, List<DocumentLine> lines) {
        try (FileWriter fw = new FileWriter("src/main/resources/" + fileName, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            for (DocumentLine line : lines) {
                out.println(line.getName() + "," + line.getDate() + "," + line.getTimezone() + "," + line.getAvailability());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
