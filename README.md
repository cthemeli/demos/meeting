# Meeting scheduler

## Save date for meetings

* Each meeting has its own file.
* To save date run file DataCollector.
* Possible actions:
	* To press 1 so as to update the content of a file (related to a specific meeting)
	* To press 2 so as to delete the content of a file
	* To press 3 in order to create a new file (with the name of the meeting)
	* To press 4 and delete an unecessary file
* All .txt files with the data are in the resources folder
* Necessary inputs
	* The action (one of the above options)
	* The name of the meeting to create/find/delete the respective file.
	* The duration of the meeting so as to create correct timezones
	* The data related to a person. Examples:
		* John can mon 15:00-17:00
		* George cant tuesday after 13:00
		* Mary can next friday before 12:00
		* Mary cannot this friday 12:00-13:00
* Possible timezones are between 09:00 and 21:00

## Find possible meetings

* For this purpose use the Calendar file
* Inputs: meeting duration and name, this or next week, if it is for the next week give the date of the week's monday